#!/bin/bash

# sort_images.sh
#
# Sort images to a target folder,
# using year, month and day from EXIF data as folder names
#
# (c) 2023 Rainer Schwarzbach
#
# License: MIT
#
# Requires:
# - exif
# - xdg-user-dir
#

set -u

SCRIPT_ABSOLUTE_PATH=$(readlink -f "$0")
SCRIPT_DIRECTORY=$(dirname "${SCRIPT_ABSOLUTE_PATH}")
source "${SCRIPT_DIRECTORY}/commons.sh"

export TARGET_BASE_DIRECTORY=$(xdg-user-dir PICTURES)


move_image_file() {
  # Move a single image file according to the date information in its metadata
  #
  full_source_path=$(readlink -f "$1")
  source_basename=$(basename "${full_source_path}")
  source_dirname=$(dirname "${full_source_path}")
  duplicates_dir="${source_dirname}.duplicates"
  image_date=$(exif --tag=DateTimeOriginal -m "${full_source_path}" 2>/dev/null)
  if [ $? -eq 0 ] ; then
    target_subdirectory=$(cut -d ' ' -f 1 <<<"$image_date" | tr ':' '/')
    target_directory="${TARGET_BASE_DIRECTORY}/${target_subdirectory}"
    full_target_path="${target_directory}/${source_basename}"
    if [ -f "${full_target_path}" ] ; then
      log_warning "Not moving ${source_basename}: it already exists in ${target_directory} ..."
      if [ "${full_source_path}" -ef "${full_target_path}" ] ; then
        log_warning "... and is the same file"
        log_warning "    → moving it to ${duplicates_dir} instead."
        ensure_directory_exists "${duplicates_dir}"
        mv "${full_source_path}" "${duplicates_dir}"
      else
        source_sha=$(sha256sum "${full_source_path}")
        target_sha=$(sha256sum "${full_target_path}")
        if [ "${source_sha}" = "${target_sha}" ] ; then
          log_warning "... and has the same SHA256 sum"
          log_warning "    → moving it to ${duplicates_dir} instead."
          ensure_directory_exists "${duplicates_dir}"
          mv "${full_source_path}" "${duplicates_dir}"
        else
          log_warning "... but seems to be different"
          log_warning "    → leaving it in place for you to investigate."
        fi
      fi
    else
      log_info "Moving ${source_basename} to ${target_directory}"
      ensure_directory_exists "${target_directory}"
      mv "${full_source_path}" "${target_directory}"
    fi
  else
    log_warning "Not moving ${source_basename}: no date information found"
  fi
}

export -f move_image_file


main() {
  # Script main function:
  # Move files from the source directory to the target directory
  SOURCE_DIRECTORY=${1:-"."}
  find "${SOURCE_DIRECTORY}" -maxdepth 1 -type f -print0 | xargs -i -0 bash -c 'move_image_file "$@"' _ {}
  exit ${RETURNCODE_OK}
}


# Main script execution

main "$@"

