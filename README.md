# Sort Images

Small scripts to move images into a target folder structure
based on the date from the images’ EXIF metadata.

## sort_images.sh

The [sort_images.sh](./sort_images.sh) script
requires the following command line utilities
which should be packaged for your Linux distribution:

- `exif` (see <https://libexif.github.io/>)
- `xdg-user-dir` (package `xdg-user-dirs`, see <https://www.freedesktop.org/wiki/Software/xdg-user-dirs/>)

Usage: `path/to/scripts/sort_images.sh [ source_directory ]`

If no _source\_directory_ is given,
the images in the current working directory are processed.

## heic2jpg.sh

The [heic2jpg.sh](./heic2jpg.sh) script can be used to convert images from the
[HEIC](https://en.wikipedia.org/wiki/High_Efficiency_Image_File_Format#HEIC:_HEVC_in_HEIF)
to the JPEG format.
It requires `exif` as well, plus one of the
following image manipulation suites
(both should also be packaged for your Linux distribution):

- [GraphicsMagick](http://www.graphicsmagick.org/)
- [ImageMagick](https://imagemagick.org/)

Usage: `path/to/scripts/heic2jpg.sh [ source_directory ]`

If no _source\_directory_ is given,
the images in the current working directory are converted.


