#!/bin/bash

# commons.sh
#
# Common constants and functions for the
# sort-images project scripts
#
# (c) 2023 Rainer Schwarzbach
#
# License: MIT
#

RETURNCODE_OK=0
RETURNCODE_ERROR=1

# Logging helper functions

log_info() {
  # Write an info message
  echo "INFO     | $@" >&2
}

log_warning() {
  # Write a warning message
  echo "WARNING  | $@" >&2
}

log_error() {
  # Write an error message
  echo "ERROR    | $@" >&2
}

exit_error () {
  # Write the error message and exit
  log_error "$@"
  exit ${RETURNCODE_ERROR}
}

# Directory creation

ensure_directory_exists() {
  # Check if a directory exists. If not, create it.
  if [ -d "$1" ] ; then
    return
  fi
  if [ -e "$1" -a ! -d "$1" ] ; then
    exit_error "$1 should be a directory, but is of another type!"
  fi
  mkdir -p "$1" || exit ${RETURNCODE_ERROR}
}

# Export functions

export -f log_info log_warning log_error exit_error ensure_directory_exists

#

