#!/bin/bash

# heic2jpg.sh
#
# Convert the provided HEIC image file to JPEG,
# or all HEIC image files in the provided directory.
#
# (c) 2023 Rainer Schwarzbach
#
# License: MIT
#
# Requires:
# - GraphicsMagick (preferred) or ImageMagick
# - exif
#

set -u

SCRIPT_ABSOLUTE_PATH=$(readlink -f "$0")
SCRIPT_DIRECTORY=$(dirname "${SCRIPT_ABSOLUTE_PATH}")
source "${SCRIPT_DIRECTORY}/commons.sh"

if gm version >/dev/null ; then
  log_info "Using GraphicsMagick"
  GM_PREFIX="gm"
elif convert -version >/dev/null ; then
  log_info "Using ImageMagick"
  GM_PREFIX=""
else
  exit_error "Neither GraphicsMagick nor ImageMagick found → exiting." 
fi

export GM_PREFIX


convert_to_jpeg() {
  # Convert the given file to JPEG and move the original to another directory
  export LANG=C
  full_source_path=$(readlink -f "$1")
  source_basename=$(basename "${full_source_path}")
  source_dirname=$(dirname "${full_source_path}")
  originals_dir="${source_dirname}.heic_originals"
  full_target_path="${full_source_path/%\.[Hh][Ee][Ii][Cc]/\.jpg}"
  if [ "${full_source_path}" != "${full_target_path}" ] ; then
    log_info "Converting ${source_basename} to $(basename "${full_target_path}") ..."
    $GM_PREFIX convert "${full_source_path}" "${full_target_path}"
    log_info "... and moving it to ${originals_dir}"
    ensure_directory_exists "${originals_dir}"
    mv "${full_source_path}" "${originals_dir}"
    # Either Apple's iPhone or the HEIC format seems to mess up the orientation:
    # the image is rotated, and the Orientation tag is kept as-is.
    # To fix that, we counter-rotate in some cases
    detected_orientation=$(exif --tag=Orientation -m "${full_target_path}")
    case "${detected_orientation}" in
      "Right-top")
        $GM_PREFIX mogrify -rotate -90 "${full_target_path}"
        ;;
      "Bottom-right")
        $GM_PREFIX mogrify -rotate 180 "${full_target_path}"
        ;;
      "Left-bottom")
        $GM_PREFIX mogrify -rotate 90 "${full_target_path}"
        ;;
    esac
  fi
}

export -f convert_to_jpeg


main() {
  # Script main function:
  # Convert *.HEIC to *.jpg, then
  # move files from the source directory to the target directory
  first_parameter=${1:-""}
  if [ -z "${first_parameter}" ] ; then
    SOURCE_DIRECTORY="."
  elif [ -f "${first_parameter}" ] ; then
    convert_to_jpeg "${first_parameter}"
    exit ${RETURNCODE_OK}
  else
    SOURCE_DIRECTORY="${first_parameter}"
  fi
  find "${SOURCE_DIRECTORY}" -maxdepth 1 -type f -name '*.[Hh][Ee][Ii][Cc]' -print0 | xargs -i -0 bash -c 'convert_to_jpeg "$@"' _ {}
  exit ${RETURNCODE_OK}
}


# Main script execution

main "$@"

